"""
    Django expects a ROOT_URLCONF. Don't actually use this for tests
    override in the tests themselves.
"""
from __future__ import unicode_literals

urlpatterns = []